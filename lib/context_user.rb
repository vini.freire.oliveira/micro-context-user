# frozen_string_literal: true

require 'bunny'
require 'json'
require 'rufus-scheduler'
require 'typhoeus'

ENV['RUBY_ENV'] = ENV.fetch('RUBY_ENV', 'development')
ENV['SHARED_RESOURCES'] = ENV.fetch('SHARED_RESOURCES', '/workspace/shared')
ENV['SCHEDULER_INTERVAL'] = ENV.fetch('SCHEDULER_INTERVAL', '5s')

# databases
require 'context_user_database'

Dir["#{Dir.pwd}/lib/services/**/*.rb"].sort.each { |file| require file }