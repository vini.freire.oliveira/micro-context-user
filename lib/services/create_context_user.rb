# frozen_string_literal: true

module Services
  class CreateContextUser
    class << self
      def call(params)
        @params = params['message']

        eval params['action']
      end

      private

      def create
        ContextUser::Create.run @params
      end

      def update
        update_user if @params['user']
        update_address if @params['address']
        # update_document if @params['document']
      end

      def update_user
        user = ContextUser::Find.by_id(@params['id'])
        nickname = @params['user']['nickname']
        avatar = @params['user']['avatar']
        
        ContextUser::Update.run(user, nickname: nickname, avatar: avatar)
      end

      def update_address
        address = ContextUser::Find.by_obj(Address, @params['address']['id'])
        number =  @params['address']['street_number']

        ContextUser::Update.run(address, street_number: number)
      end

      def update_document
        document = ContextUser::Find.by_obj(Document, @params['id'])

        ContextUser::Update.run(document, @params['document'])
      end

      def delete
        user = ContextUser::Find.by_id(@params['id'])

        ContextUser::Delete.run(user)
      end
    end
  end
end