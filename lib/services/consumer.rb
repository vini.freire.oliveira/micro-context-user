# frozen_string_literal: true

module Services
  class Consumer
    class << self
      def call
        conn = Bunny.new(host: ENV['RABBITMQ_HOST'])
        conn.start

        channel = conn.create_channel
        queue = channel.queue("context-user")

        queue.subscribe do |delivery_info, properties, payload|
          Services::CreateContextUser.call(JSON.parse(payload))
        end
        
        conn.stop
      end
    end
  end
end