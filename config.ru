#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative './lib/context_user.rb'

module Scheduler
  scheduler = Rufus::Scheduler.new

  scheduler.every ENV['SCHEDULER_INTERVAL'] do
    Services::Consumer.call
  end

  scheduler.join
end
